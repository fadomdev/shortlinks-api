import dotenv from 'dotenv'
import mysql from 'mysql2/promise'
dotenv.config()

const getConnection = async () => {
  // Create the connection to the database
  return await mysql.createConnection(process.env.DATABASE_URL)
}

export default getConnection
