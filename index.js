import express from 'express'
import bodyParser from 'body-parser'
import { nanoid } from 'nanoid'
import getConnection from './connection.js'

const app = express()
const PORT = process.env.PORT || 3000

app.use(bodyParser.json())

app.get('/hello', (req, res) => {
  res.send('Hello World!')
})

app.get('/show-shortlinks', async (req, res) => {
  const connection = await getConnection()
  const [rows] = await connection.query('SELECT * FROM shortlinks')

  //console.log(results) // results contains rows returned by server
  await connection.end()
  res.send({ data: rows })
})

// Route to shorten a URL
app.post('/shorten', async (req, res) => {
  const connection = await getConnection()
  const { originalUrl } = req.body

  // Check if the URL is valid
  if (!isValidUrl(originalUrl)) {
    return res.status(400).json({ error: 'Invalid URL' })
  }

  const shortUrl = nanoid(5)

  try {
    // Check if the URL already exists in the database
    const [rows] = await connection.query(
      `SELECT * FROM shortlinks WHERE original_url = '${originalUrl}'`
    )

    console.log(rows)

    if (rows.length === 0) {
      // If the URL doesn't exist, insert it into the database
      await connection.query(
        'INSERT INTO shortlinks (original_url, short_key) VALUES (?, ?)',
        [originalUrl, shortUrl]
      )

      await connection.end()
    }

    return res.json({ shortUrl: `${req.get('host')}/${shortUrl}` })
  } catch (error) {
    console.error(error)
    res.status(500).json({ error: 'Internal Server Error' })
  }
})

// Route to redirect to the original URL
app.get('/:shortUrl', async (req, res) => {
  const connection = await getConnection()
  const { shortUrl } = req.params

  try {
    // Retrieve the original URL from the database
    const [rows] = await connection.query(
      'SELECT * FROM shortlinks WHERE short_key = ?',
      [shortUrl]
    )

    if (rows.length > 0) {
      await connection.end()
      return res.redirect(rows[0].original_url)
    } else {
      await connection.end()
      return res.status(404).json({ error: 'URL not found' })
    }
  } catch (error) {
    console.error(error)
    res.status(500).json({ error: 'Internal Server Error' })
  }
})

// Check if a URL is valid
function isValidUrl(url) {
  try {
    new URL(url)
    return true
  } catch (error) {
    return false
  }
}

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`)
})
